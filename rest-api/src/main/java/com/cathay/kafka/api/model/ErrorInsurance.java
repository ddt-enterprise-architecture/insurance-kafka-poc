package com.cathay.kafka.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorInsurance {
    private int code;
    private String msg;
    private Insurance insurance;
}

