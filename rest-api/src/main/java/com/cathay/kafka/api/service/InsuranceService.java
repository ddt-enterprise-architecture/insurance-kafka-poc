package com.cathay.kafka.api.service;

import com.cathay.kafka.api.event.InsuranceSender;
import com.cathay.kafka.api.model.ErrorInsurance;
import com.cathay.kafka.api.model.Insurance;
import com.cathay.kafka.api.model.InsuranceList;
import com.cathay.kafka.api.utils.Topic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rich
 */
@Slf4j
@Service
public class InsuranceService {

    @Autowired
    InsuranceSender insuranceSender;

    private List<ErrorInsurance> errorInsuranceList = new ArrayList<>();

    private List<Insurance> processedInsuranceList = new ArrayList<>();

    public void send(Insurance insurance) {
        insuranceSender.send(insurance);
    }

    public void send(InsuranceList insuranceList) {
        insuranceSender.sendInTransaction(insuranceList);
    }

    public List<ErrorInsurance> getErrorInsuranceList() {
        return errorInsuranceList;
    }

    public List<Insurance> getProcessedInsuranceList() {
        return processedInsuranceList;
    }

    @KafkaListener(id = "insurance-consumer", topics = Topic.INSURANCE_PROCESSED, groupId = "insurance-processed-group", containerFactory = "kafkaListenerFactory")
    public void processedInsuranceListener(Insurance insurance, Acknowledgment acknowledgment) {
        log.info("received processed insurance: {}", insurance);
        processedInsuranceList.add(insurance);

        // Committing the offset to Kafka manually.
        acknowledgment.acknowledge();
    }

    @KafkaListener(id = "error-consumer", topics = Topic.INSURANCE_ERROR, groupId = "insurance-error-group", containerFactory = "kafkaErrorListenerFactory")
    public void processedErrorListener(ErrorInsurance errorInsurance) throws InterruptedException {
        log.info("received error: {}", errorInsurance);
        errorInsuranceList.add(errorInsurance);
    }
}
