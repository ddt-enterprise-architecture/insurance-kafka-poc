package com.cathay.kafka.api.controller;

import com.cathay.kafka.api.model.ErrorInsurance;
import com.cathay.kafka.api.model.Insurance;
import com.cathay.kafka.api.model.InsuranceList;
import com.cathay.kafka.api.service.InsuranceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author rich
 */
@Slf4j
@RestController
@RequestMapping("/insurance")
public class InsuranceController {

    @Autowired
    private InsuranceService insuranceService;

    @PostMapping
    public void createInsurance(@RequestBody Insurance insurance) {
        insuranceService.send(insurance);
    }

    @PostMapping("/list")
    public void createInsurances(@RequestBody InsuranceList insuranceList) {
        insuranceService.send(insuranceList);
    }

    @GetMapping
    public List<Insurance> getProcessedInsurance() {
        return insuranceService.getProcessedInsuranceList();
    }

    @GetMapping("/error")
    public List<ErrorInsurance> getErrorMessages() {
        return insuranceService.getErrorInsuranceList();
    }

}
