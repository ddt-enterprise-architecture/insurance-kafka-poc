package com.cathay.kafka.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Insurance {
    private String id;
    private String preKey;
    private String policyNo;
    private int serNo;
    private int productCode;
    private String productPolicyCode;
    private int insuredIdKind;
    private String insuredId;
}

