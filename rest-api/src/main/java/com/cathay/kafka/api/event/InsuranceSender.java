package com.cathay.kafka.api.event;

import com.cathay.kafka.api.model.Insurance;
import com.cathay.kafka.api.model.InsuranceList;
import com.cathay.kafka.api.utils.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.concurrent.TimeUnit;

/**
 * @author rich
 */
@Slf4j
@Service
public class InsuranceSender {

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Autowired
    KafkaTemplate kafkaTemplate;

    public void sendAsync(Insurance insurance) {
        ListenableFuture<SendResult<String, Insurance>> future = kafkaTemplate.send(
                Topic.INSURANCE_CREATED, insurance.getId(), insurance);

        future.addCallback(new ListenableFutureCallback<SendResult<String, Insurance>>() {
            @Override
            public void onSuccess(SendResult<String, Insurance> result) {
                ProducerRecord<String, Insurance> record = result.getProducerRecord();
                log.info("send insurance success, key: {}, value: {}", record.key(), record.value());
                RecordMetadata recordMetadata = result.getRecordMetadata();
                log.info("Metadata - Topic: {}, Partition: {}, Offset: {} ",
                        recordMetadata.topic(),
                        recordMetadata.partition(),
                        recordMetadata.offset());
            }

            @Override
            public void onFailure(Throwable ex) {
                log.error("send insurance failed!", ex);
            }

        });
    }

    public void send(Insurance insurance) {
        kafkaTemplate.send(Topic.INSURANCE_CREATED, insurance.getId(), insurance);
    }

    /**
     * Enable Transaction
     * @param insuranceList
     */
    public void send(InsuranceList insuranceList) {
        kafkaTemplate.executeInTransaction(t -> {
            insuranceList.getData().forEach(i -> {
                t.send(Topic.INSURANCE_CREATED, i.getId(), i);
                log.info("send message to kafka, key: {}", i.getId());
                sleep();
            });
            return true;
        });
    }

    @Transactional
    public void sendInTransaction(InsuranceList insuranceList) {
        insuranceList.getData().forEach(i -> {
            kafkaTemplate.send(Topic.INSURANCE_CREATED, i.getId(), i);
            log.info("send message to kafka, key: {}", i.getId());
            sleep();
        });
    }

    private void sleep() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            log.error("Unexpected error!", e);
        }
    }
}
