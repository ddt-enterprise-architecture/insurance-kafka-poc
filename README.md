# insurance-kafka-poc

## The purpose of the project
- ensure exactly-once delivery between producer and consumer
- enable [Transactional](https://docs.spring.io/spring-kafka/reference/html/#transactions)

## Architecture
<img src="./doc/kafka-poc-architecture.png" alt="drawing" width="900" />

- Services:
  - [REST API](http://localhost:8081/api/swagger-ui.html)
  - Insurnace processor


## Enable Transaction & Exactly-Once

### Enable Transaction
- Producer
- Transactions are enabled by providing the DefaultKafkaProducerFactory with a transactionIdPrefix

```java
DefaultKafkaProducerFactory producerFactory = new DefaultKafkaProducerFactory<>(configProps);
producerFactory.setTransactionIdPrefix("insurance-tx");
return producerFactory;
```

- The transaction can be started by a TransactionTemplate, a @Transactional method

```java
@Transactional
public void sendInTransaction(InsuranceList insuranceList) {
    insuranceList.getData().forEach(i -> {
        kafkaTemplate.send(Topic.INSURANCE_CREATED, i.getId(), i);
        log.info("send message to kafka, key: {}", i.getId());
        sleep();
    });
}
```

- Init KafkaTransactionManager in Spring Configuration
```java
@Bean
public KafkaTransactionManager transactionManager(ProducerFactory producerFactory) {
    KafkaTransactionManager manager = new KafkaTransactionManager(producerFactory);
    return manager;
}
```


### Enable Exactly-Once
- Consumer  

  - In read_committed mode, the consumer will read only those transactional messages which have been successfully committed

```java
configProps.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
```

  - Disable auto-commit offset and commit Specified offset manually

```java
ConcurrentKafkaListenerContainerFactory<String, Insurance> factory = new ConcurrentKafkaListenerContainerFactory<>();
factory.setConsumerFactory(consumerFactory());
factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL_IMMEDIATE);
factory.getContainerProperties().setSyncCommits(true);
return factory;
```

## POC items
- Transaction 
  - Produce data
  - Consume data 
- Exactly-once semantic(Consumer)
- Error message reply

### Error code

|         | Validation Error                      | DB Insertion Error             |
|---------|---------------------------------------|--------------------------------|
| Code    | 406                                   | 501                            |
| Message | Insurance validation error, field: <> | DB insertion error, caused: <> |

## Reference
- [Spring Kafka Transaction](https://docs.spring.io/spring-kafka/reference/html/#transactions)
- [Spring for Apache Kafka Transaction Support](https://www.confluent.io/blog/spring-for-apache-kafka-deep-dive-part-1-error-handling-message-conversion-transaction-support/)
- [Exactly Once Processing in Kafka](https://www.baeldung.com/kafka-exactly-once)
