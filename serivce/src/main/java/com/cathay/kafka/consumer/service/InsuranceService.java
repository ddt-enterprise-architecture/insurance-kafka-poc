package com.cathay.kafka.consumer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import com.cathay.kafka.consumer.dao.InsuranceMapper;
import com.cathay.kafka.consumer.enums.InsuranceStatus;
import com.cathay.kafka.consumer.event.InsuranceSender;
import com.cathay.kafka.consumer.model.Insurance;
import com.cathay.kafka.consumer.utils.Topic;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @author rich
 */
@Slf4j
@Service
public class InsuranceService {

    @Autowired
    InsuranceSender insuranceSender;

    @Autowired
    private InsuranceMapper mapper;

    @KafkaListener(id = "insurance-created-consumer", topics = Topic.INSURANCE_CREATED, groupId = "insurance-created-group", containerFactory = "kafkaListenerFactory")
    public void processedInsuranceListener(Insurance insurance, Acknowledgment acknowledgment) throws InterruptedException {
        log.info("received insurance: {}", insurance);
        String errMsg = checkInsurance(insurance);
        TimeUnit.SECONDS.sleep(3);
        if (errMsg != null) {
            insuranceSender.sendError(insurance, InsuranceStatus.VALIDATION_ERROR, errMsg);
        } else {
            try {
                Insurance newInsurance = insertInsurance(insurance);
                insuranceSender.sendProcessed(newInsurance);
            } catch (Exception e) {
                log.error("insert error " + e.getLocalizedMessage(), e);
                insuranceSender.sendError(insurance, InsuranceStatus.DB_Insertion_ERROR, e.getMessage());
            }

        }
        acknowledgment.acknowledge();
    }

    private String checkInsurance(Insurance insurance) {
        if (insurance == null) {
            return "insurance is null";
        }
        String id = insurance.getId();
        if (id == null || id.length() == 0) {
            return "id is required";
        }
        String preKey = insurance.getPreKey();
        if (preKey == null || preKey.length() == 0) {
            return "preKey is required";
        }
        String policyNo = insurance.getPolicyNo();
        if (policyNo == null || policyNo.length() == 0) {
            return "policyNo is required";
        }
        return null;
    }

    private Insurance insertInsurance(Insurance insurance) {
        log.info("insert insurance: {}", insurance);
        mapper.insert(insurance);
        return insurance;
    }


}
