package com.cathay.kafka.consumer.utils;

public class Topic {
    public static final String INSURANCE_CREATED = "streaming.insurance.created";
    public static final String INSURANCE_PROCESSED = "streaming.insurance.processed";
    public static final String INSURANCE_ERROR = "streaming.insurance.error";
}
