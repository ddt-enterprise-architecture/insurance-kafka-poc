package com.cathay.kafka.consumer.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.cathay.kafka.consumer.enums.InsuranceStatus;
import com.cathay.kafka.consumer.model.ErrorInsurance;
import com.cathay.kafka.consumer.model.Insurance;
import com.cathay.kafka.consumer.utils.Topic;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author rich
 */
@Slf4j
@Service
public class InsuranceSender {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Autowired
    private KafkaTemplate kafkaCommonTemplate;

    @Transactional
    public void sendProcessed(Insurance insurance) {
        log.info("send processed: {}", insurance);
        kafkaTemplate.send(Topic.INSURANCE_PROCESSED, insurance.getId(), insurance);
    }

    public void sendError(Insurance insurance, InsuranceStatus status, String errMsg) {
        log.info("send error: insurance: {}, code: {}, errMsg: {}", insurance, status.getCode(), errMsg);
        ErrorInsurance errorInsurance = ErrorInsurance.builder()
                .code(status.getCode())
                .msg(status.getMessage().replace("<>", errMsg))
                .insurance(insurance)
                .build();
        try {
            kafkaCommonTemplate.send(Topic.INSURANCE_ERROR, errorInsurance);
        } catch (Exception e) {
            log.error("Send message: {} to error topic: {}, failed!", errorInsurance, Topic.INSURANCE_ERROR);
        }

    }
}
