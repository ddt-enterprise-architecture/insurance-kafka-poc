package com.cathay.kafka.consumer.enums;

public enum InsuranceStatus {
    VALIDATION_ERROR(406, "Insurance validation error, field: <>"),
    DB_Insertion_ERROR(501, "DB insertion error, caused: <>");

    private int code;
    private String message;

    InsuranceStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
