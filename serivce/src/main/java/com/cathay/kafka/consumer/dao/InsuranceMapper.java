package com.cathay.kafka.consumer.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

import com.cathay.kafka.consumer.model.Insurance;

@Mapper
public interface InsuranceMapper {


    @Insert("INSERT INTO INSURANCE (ID, PRE_KEY, POLICY_NO, SER_NO, PRODUCT_CODE, PRODUCT_POLICY_CODE, INSURED_ID_KIND, INSURED_ID) " +
            "VALUES (" +
            "#{insurance.id}, " +
            "#{insurance.preKey}, " +
            "#{insurance.policyNo}, " +
            "#{insurance.serNo}, " +
            "#{insurance.productCode}, " +
            "#{insurance.productPolicyCode}, " +
            "#{insurance.insuredIdKind}, " +
            "#{insurance.insuredId})")
    void insert(@Param("insurance") Insurance insurance);

}
