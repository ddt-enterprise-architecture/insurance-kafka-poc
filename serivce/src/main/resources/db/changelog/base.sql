--liquibase formatted sql logicalFilePath:db.changelog-base

/* @formatter:off */

--changeset base:1 context:base
CREATE TABLE INSURANCE (
  id           BIGINT PRIMARY KEY,
  PRE_KEY     VARCHAR(255) NOT NULL,
  POLICY_NO     VARCHAR(255) NOT NULL,
  SER_NO        INTEGER NOT NULL,
PRODUCT_CODE        CHARACTER(1) ,
PRODUCT_POLICY_CODE        VARCHAR(7) ,
INSURED_ID_KIND        CHARACTER(1) ,
INSURED_ID        VARCHAR(20)
);
--rollback drop table INSURANCE;
