`docker exec -it mysql bash`

`mysql -u mysqluser -p`

###Create table
````
CREATE TABLE INSURANCE (
    id           BIGINT AUTO_INCREMENT PRIMARY KEY,
    PRE_KEY     VARCHAR(255) NOT NULL,
    POLICY_NO     VARCHAR(255) NOT NULL,
    SER_NO        INTEGER NOT NULL,
    PRODUCT_CODE        CHARACTER(1) ,
    PRODUCT_POLICY_CODE        VARCHAR(7) ,
    INSURED_ID_KIND        CHARACTER(1) ,
    INSURED_ID        VARCHAR(20) 
);
````